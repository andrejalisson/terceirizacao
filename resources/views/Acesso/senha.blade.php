<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Nova Senha | A.S. Terceirização</title>

    <link href="/admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="/admin/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="admin/js/izi/dist/css/iziToast.min.css">
    <link href="/admin/css/animate.css" rel="stylesheet">
    <link href="/admin/css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <h3>Crie uma nova senha.</h3>
            <p>Lembre-se: Não divida sua senha com ninguém, seu acesso é monitorado e tudo realizado com seu login e de sua responsabilidade.</p>
            <form class="m-t" role="form" method="POST" action="/Recuperar">
                {!! csrf_field() !!}
                <input type="hidden" name="token" value="{{$token}}">
                <div class="form-group">
                    <input type="password" class="form-control" name="senha" placeholder="Nova Senha" required="">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Salvar</button>
            </form>
            <p class="m-t"> <small>Grupo Andrade Serviços &copy; 1995 - {{date('Y')}}</small> </p>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="/admin/js/jquery-3.1.1.min.js"></script>
    <script src="/admin/js/popper.min.js"></script>
    <script src="/admin/js/bootstrap.js"></script>
    <script src="/admin/js/izi/dist/js/iziToast.min.js" type="text/javascript"></script>


        @if (session('sucesso'))
        <script>
            iziToast.show({
                title: 'Sucesso',
                message: "{{session('sucesso')}}"
            });
        </script>
        @endif
        @if (session('erro'))
        <script>
            iziToast.show({
                title: 'Erro',
                message: "{{session('erro')}}"
            });
        </script>
        @endif
        @if (session('atencao'))
        <script>
            iziToast.show({
                title: 'Atenção',
                message: "{{session('atencao')}}"
            });
        </script>
        @endif
        @if (session('informacao'))
        <script>
            iziToast.show({
                title: 'Informação',
                message: "{{session('informacao')}}"
            });
        </script>
        @endif
</body>

</html>
