<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Esqueceu a Senha | A.S. Terceirização</title>

    <link href="/admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="/admin/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="admin/js/izi/dist/css/iziToast.min.css">
    <link href="/admin/css/animate.css" rel="stylesheet">
    <link href="/admin/css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <h3>Esqueceu a senha?</h3>
            <p>Coloque seu email de acesso para contunuar, logo você receberá um email para alterar sua senha de acesso.</p>
            <form class="m-t" role="form" method="POST" action="/Recuperar">
                {!! csrf_field() !!}
                <div class="form-group">
                    <input type="email" class="form-control" name="email" placeholder="E-mail" required="">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Enviar</button>
            </form>
            <p class="m-t"> <small>Grupo Andrade Serviços &copy; 1995 - {{date('Y')}}</small> </p>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="/admin/js/jquery-3.1.1.min.js"></script>
    <script src="/admin/js/popper.min.js"></script>
    <script src="/admin/js/bootstrap.js"></script>
    <script src="/admin/js/izi/dist/js/iziToast.min.js" type="text/javascript"></script>


        @if (session('sucesso'))
        <script>
            iziToast.show({
                title: 'Sucesso',
                message: "{{session('sucesso')}}"
            });
        </script>
        @endif
        @if (session('erro'))
        <script>
            iziToast.show({
                title: 'Erro',
                message: "{{session('erro')}}"
            });
        </script>
        @endif
        @if (session('atencao'))
        <script>
            iziToast.show({
                title: 'Atenção',
                message: "{{session('atencao')}}"
            });
        </script>
        @endif
        @if (session('informacao'))
        <script>
            iziToast.show({
                title: 'Informação',
                message: "{{session('informacao')}}"
            });
        </script>
        @endif
</body>

</html>
