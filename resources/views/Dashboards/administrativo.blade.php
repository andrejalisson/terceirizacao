@extends('templates.sistema')

@section('css')

@endsection

@section('inline')

@endsection

@section('corpo')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-content">
                <div>
                    <span class="float-right text-right">
                    <small>Balanço baseado nas movimentações  <strong>financeiras</strong></small>
                        <br/>
                        Saldo Atual: R$ 162.862,00
                    </span>
                    <h1 class="m-b-xs">R$ 21.321,12</h1>
                    <h3 class="font-bold no-margins">
                        Balando do mês(<small>até o momento</small>)
                    </h3>
                    <small>Sujeito a alterações até o final do dia.</small>
                </div>
                <div>
                    <canvas id="lineChart" height="100"></canvas>
                </div>
                <div class="m-t-md">
                    <small class="float-right">
                        <i class="fa fa-clock-o"> </i>
                        Atualizado em {{date("d.m.Y H:i:s")}}
                    </small>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<!-- Flot -->
<script src="/admin/js/plugins/flot/jquery.flot.js"></script>
<script src="/admin/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
<script src="/admin/js/plugins/flot/jquery.flot.spline.js"></script>
<script src="/admin/js/plugins/flot/jquery.flot.resize.js"></script>
<script src="/admin/js/plugins/flot/jquery.flot.pie.js"></script>
<script src="/admin/js/plugins/flot/jquery.flot.symbol.js"></script>
<script src="/admin/js/plugins/flot/curvedLines.js"></script>

<!-- Peity -->
<script src="/admin/js/plugins/peity/jquery.peity.min.js"></script>
<script src="/admin/js/demo/peity-demo.js"></script>

<!-- Custom and plugin javascript -->
<script src="/admin/js/inspinia.js"></script>
<script src="/admin/js/plugins/pace/pace.min.js"></script>

<!-- jQuery UI -->
<script src="/admin/js/plugins/jquery-ui/jquery-ui.min.js"></script>

<!-- Jvectormap -->
<script src="/admin/js/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>
<script src="/admin/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>

<!-- Sparkline -->
<script src="/admin/js/plugins/sparkline/jquery.sparkline.min.js"></script>

<!-- Sparkline demo data  -->
<script src="/admin/js/demo/sparkline-demo.js"></script>

<!-- ChartJS-->
<script src="/admin/js/plugins/chartJs/Chart.min.js"></script>
@endsection

@section('script')
<script>
    $(document).ready(function() {

        var lineData = {
            labels: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
            datasets: [
                {
                    label: "Entradas",
                    backgroundColor: "rgba(26,179,148,0.5)",
                    borderColor: "rgba(26,179,148,0.7)",
                    pointBackgroundColor: "rgba(26,179,148,1)",
                    pointBorderColor: "#fff",
                    data: [25, 50, 40, 19, 86, 27, 90, 25, 50, 40, 19, 86]
                },
                {
                    label: "Saidas",
                    backgroundColor: "rgba(255,99,71,0.5)",
                    borderColor: "rgba(255,99,71,1)",
                    pointBackgroundColor: "rgba(255,0,0,1)",
                    pointBorderColor: "#fff",
                    data: [65, 59, 80, 81, 56, 55, 40, 65, 59, 80, 81, 56]
                }
            ]
        };

        var lineOptions = {
            responsive: true
        };


        var ctx = document.getElementById("lineChart").getContext("2d");
        new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});

    });
</script>
@endsection
