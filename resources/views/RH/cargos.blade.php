@extends('templates.sistema')

@section('css')
<link href="/admin/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
@endsection

@section('inline')

@endsection

@section('corpo')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Cargos e Funções</h2>
    </div>
    <div class="col-sm-8">
        <div class="title-action">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addCargo">
                Adicionar Cargo/Função
            </button>
        </div>
    </div>
</div>
<div class="modal inmodal" id="addCargo" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-laptop modal-icon"></i>
                <h4 class="modal-title">Adicionar Cargo/Função</h4>
            </div>
            <form action="">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Cargo/Função</label>
                        <input type="text" placeholder="Nome do Cargo/Função" class="form-control">
                        <label>Salário</label>
                        <input type="number" min="0.00" max="10000.00" step="0.01" placeholder="Só Números" class="form-control">
                        <label>Carga Horária</label>
                        <input type="number" placeholder="Carga Horária" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Salvar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-content">
                <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id="cargos" >
                <thead>
                <tr>
                    <th>CBO</th>
                    <th>Cargo/Função</th>
                    <th>Salário <small>(Sugerido.)</small></th>
                    <th>Escala/Carga Horária</th>
                    <th>Valor Por Hora</th>
                    <th>Opções</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
                </div>

            </div>
        </div>
    </div>
    </div>
</div>
@endsection

@section('js')
    <script src="/admin/js/plugins/dataTables/datatables.min.js"></script>
    <script src="/admin/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>


@endsection

@section('script')
<script>

    $(function () {
        $(document).ready(function () {
            // Initialize Example 2
            $('#cargos').dataTable({
                "processing": true,
                "order": [[ 1, "asc" ]],
                "serverSide": true,
                "oLanguage": {
                "sLengthMenu": "Mostrar _MENU_ registros por página",
                "sZeroRecords": "Nenhum registro encontrado",
                "sInfo": "Mostrando _END_ de _TOTAL_ registro(s)",
                "sInfoEmpty": "Mostrando 0 / 0 de 0 registros",
                "sInfoFiltered": "(filtrado de _MAX_ registros)",
                "sSearch": "Pesquisar: ",
                "oPaginate": {
                    "sFirst": "Início",
                    "sPrevious": "Anterior",
                    "sNext": "Próximo",
                    "sLast": "Último"
                    }
                },
                "ajax":{
                     "url": "{{ url('todasCargos') }}",
                     "dataType": "json",
                     "type": "POST",
                     "data":{
                            _token: "{{csrf_token()}}"

                     }
                   },
                "columns": [
                    { "data": "codigo" },
                    { "data": "nome" },
                    { "data": "salario" },
                    { "data": "horario" },
                    { "data": "valorH" },
                    { "data": "opcoes" }
            ]
            });
        });
    });

</script>
@endsection
