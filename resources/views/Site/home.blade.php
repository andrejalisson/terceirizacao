<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>{{$title}} | A.S Terceirização</title>
        <!-- Stylesheets -->
        <link href="/css/bootstrap.css" rel="stylesheet">
        <link href="/css/revolution-slider.css" rel="stylesheet">
        <link href="/css/style.css" rel="stylesheet">

        <!--Favicon-->
        <link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/images/favicon.ico" type="image/x-icon">
        <!-- Responsive -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <link href="/css/responsive.css" rel="stylesheet">
        <!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
        <!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
    </head>
<body>
    <div class="page-wrapper">
        <div class="preloader"></div>
        <header class="main-header">
            <div class="header-upper">
                <div class="auto-container">
                    <div class="clearfix">
                        <div class="pull-left logo-outer">
                            <div class="logo"><a href="/"><img src="/images/logo.png" alt="" title="A.S Terceirização"></a></div>
                        </div>

                        <div class="pull-right upper-right clearfix">

                            <!--Info Box-->
                            <div class="upper-column info-box">
                                <div class="icon-box"><span class="flaticon-clock-2"></span></div>
                                HORÁRIO DE ATENDIMENTO
                                <div class="light-text">Segunda à Sexta: 7h às 17h</div>
                            </div>

                            <!--Info Box-->
                            <div class="upper-column info-box">
                                <div class="icon-box"><span class="flaticon-technology-1"></span></div>
                                TELEFONE
                                <div class="light-text">(85) 3271-0110</div>
                            </div>

                            <!--Info Box-->
                            <div class="upper-column info-box">
                                <div class="icon-box"><span class="flaticon-envelope-3"></span></div>
                                E-MAIL
                                <div class="light-text">contato@asterceirizacao.com.br</div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

            <!--Header-Lower-->
            <div class="header-lower">

                <div class="auto-container">
                    <div class="nav-outer clearfix">
                        <!-- Main Menu -->
                        <nav class="main-menu">
                            <div class="navbar-header">
                                <!-- Toggle Button -->
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>

                            <div class="navbar-collapse collapse clearfix">
                                <ul class="navigation clearfix">
                                    <li><a href="#">Home</a></li>
                                    <li><a href="#empresa">A Empresa</a></li>
                                    <li><a href="#servicos">Serviços</a></li>
                                    <li><a href="#orcamento">Orçamentos</a></li>
                                    <li><a href="#">Trabalhe Conosco</a></li>
                                    <li><a href="#">Contato</a></li>
                                </ul>
                            </div>
                        </nav>
                        <!-- Main Menu End-->

                        <div class="outer-btn"><a href="#orcamento" class="theme-btn consultation">SOLICITE SEU ORÇAMENTO</a></div>

                    </div>
                </div>
            </div>

            <!--Sticky Header-->
            <div class="sticky-header">
                <div class="auto-container clearfix">
                    <!--Logo-->
                    <div class="logo pull-left">
                        <a href="/" class="img-responsive"><img src="/images/logo-small.png" alt="A.S. Terceirização"></a>
                    </div>

                    <!--Right Col-->
                    <div class="right-col pull-right">
                        <!-- Main Menu -->
                        <nav class="main-menu">
                            <div class="navbar-header">
                                <!-- Toggle Button -->
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>

                            <div class="navbar-collapse collapse clearfix">
                                <ul class="navigation clearfix">
                                    <li><a href="#">Home</a></li>
                                    <li><a href="#empresa">A Empresa</a></li>
                                    <li><a href="#servicos">Serviços</a></li>
                                    <li><a href="#orcamento">Orçamentos</a></li>
                                    <li><a href="#">Trabalhe Conosco</a></li>
                                    <li><a href="#">Contato</a></li>
                                </ul>
                            </div>
                        </nav><!-- Main Menu End-->
                    </div>

                </div>
            </div>
            <!--End Sticky Header-->

        </header>
        <!--End Main Header -->

        <!--Main Slider-->
        <section class="main-slider" data-start-height="820" data-slide-overlay="yes">

            <div class="tp-banner-container">
                <div class="tp-banner">
                    <ul>
                        <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="/images/main-slider/1.jpg"  data-saveperformance="off"  data-title="Awesome Title Here">
                        <img src="/images/main-slider/1.jpg"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">

                            <div class="tp-caption sft sfb tp-resizeme"
                            data-x="left" data-hoffset="15"
                            data-y="center" data-voffset="-60"
                            data-speed="1500"
                            data-start="0"
                            data-easing="easeOutExpo"
                            data-splitin="none"
                            data-splitout="none"
                            data-elementdelay="0.01"
                            data-endelementdelay="0.3"
                            data-endspeed="1200"
                            data-endeasing="Power4.easeIn">
                                <div class="text">Soluções de terceirização para seu negócio.</div>
                            </div>

                            <div class="tp-caption sft sfb tp-resizeme"
                            data-x="left" data-hoffset="15"
                            data-y="center" data-voffset="20"
                            data-speed="1500"
                            data-start="500"
                            data-easing="easeOutExpo"
                            data-splitin="none"
                            data-splitout="none"
                            data-elementdelay="0.01"
                            data-endelementdelay="0.3"
                            data-endspeed="1200"
                            data-endeasing="Power4.easeIn">
                                <h2>LIMPEZA, SEGURANÇA,<br>MOTORISTAS, OPERÁRIOS E MAIS.</h2>
                            </div>

                            <div class="tp-caption sft sfb tp-resizeme"
                            data-x="left" data-hoffset="15"
                            data-y="center" data-voffset="110"
                            data-speed="1500"
                            data-start="1000"
                            data-easing="easeOutExpo"
                            data-splitin="none"
                            data-splitout="none"
                            data-elementdelay="0.01"
                            data-endelementdelay="0.3"
                            data-endspeed="1200"
                            data-endeasing="Power4.easeIn">
                                <div class="btn-box"><a href="#orcamento" class="theme-btn btn-style-one">SOLICITE SEU ORÇAMENTO</a></div>
                            </div>

                        </li>

                        <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="images/main-slider/2.jpg"  data-saveperformance="off"  data-title="Awesome Title Here">
                        <img src="/images/main-slider/2.jpg"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">

                            <div class="tp-caption sft sfb tp-resizeme"
                            data-x="center" data-hoffset="0"
                            data-y="center" data-voffset="-60"
                            data-speed="1500"
                            data-start="0"
                            data-easing="easeOutExpo"
                            data-splitin="none"
                            data-splitout="none"
                            data-elementdelay="0.01"
                            data-endelementdelay="0.3"
                            data-endspeed="1200"
                            data-endeasing="Power4.easeIn">
                                <div class="text"> </div>
                            </div>

                            <div class="tp-caption sft sfb tp-resizeme text-center"
                            data-x="center" data-hoffset="0"
                            data-y="center" data-voffset="20"
                            data-speed="1500"
                            data-start="500"
                            data-easing="easeOutExpo"
                            data-splitin="none"
                            data-splitout="none"
                            data-elementdelay="0.01"
                            data-endelementdelay="0.3"
                            data-endspeed="1200"
                            data-endeasing="Power4.easeIn">
                                <h2>TERCEIRIZAÇÃO DE SERVIÇOS COM <br>CONFIANÇA E QUALIDADE.</h2>
                            </div>

                            <div class="tp-caption sft sfb tp-resizeme"
                            data-x="center" data-hoffset="0"
                            data-y="center" data-voffset="110"
                            data-speed="1500"
                            data-start="1000"
                            data-easing="easeOutExpo"
                            data-splitin="none"
                            data-splitout="none"
                            data-elementdelay="0.01"
                            data-endelementdelay="0.3"
                            data-endspeed="1200"
                            data-endeasing="Power4.easeIn">
                                <div class="btn-box"><a href="#orcamento" class="theme-btn btn-style-one">Solicite seu Orçamento.</a></div>
                            </div>
                        </li>
                    </ul>
                    <div class="tp-bannertimer"></div>
                </div>
            </div>
        </section>
        <!--End Main Slider-->

        <!--Inquiry Form-->
        <section id="orcamento" class="inquiry-section wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
            <div class="auto-container">

                <!--Inquiry Form-->
                <div class="inquiry-form">
                    <form method="post">
                        <div class="row clearfix">

                            <!--Form Group-->
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <div class="group-inner">
                                    <label class="icon-label" for="field-one"><span class="flaticon-social"></span></label>
                                    <input id="field-one" type="text" name="nome" value="" placeholder="Nome" required>
                                </div>
                            </div>
                            <!--Form Group-->
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <div class="group-inner">
                                    <label class="icon-label" for="field-two"><span class="flaticon-business-1"></span></label>
                                    <input id="field-two" type="email" name="email" value="" placeholder="Email" required>
                                </div>
                            </div>
                            <!--Form Group-->
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <div class="group-inner">
                                    <label class="icon-label" for="field-three"><span class="flaticon-smartphone"></span></label>
                                    <input id="field-three" type="text" name="phone" placeholder="Telefone">
                                </div>
                            </div>
                            <!--Form Group-->
                            <div class="form-group col-md-4 col-sm-12 col-xs-12">
                                <div class="group-inner">
                                    <label class="icon-label" for="field-four"><span class="flaticon-speech-bubble-1"></span></label>
                                    <textarea id="field-four" name="message" placeholder="Conte-nos sobre sua necessidade."></textarea>
                                </div>
                            </div>
                            <!--Form Group-->
                            <div class="form-group col-md-4 col-sm-12 col-xs-12">
                                <button type="submit" class="theme-btn btn-style-three">Enviar <span class="fa fa-arrow-right"></span></button>
                            </div>

                        </div>
                    </form>
                </div>
                <!--End Inquiry Form-->

            </div>
        </section>
        <!--End Inquiry Form-->

        <!--Services Section-->
        <section id="servicos" class="services-section">
            <div class="auto-container">
                <!--Sec Title-->
                <div class="sec-title">
                    <h2>NOSSOS MELHORES SERVIÇOS</h2>
                    <div class="text">Profissionais constantemente treinados para melhor servir.</div>
                </div>

                <div class="outer-box" style="background-image: url(/images/background/dotted-map.png);">
                    <div class="row clearfix">

                        <!--Left Column-->
                        <div class="left-column col-md-4 col-sm-6 col-xs-12 pull-left">
                            <!--Service Block One-->
                            <div class="service-block-one">
                                <div class="inner-box">
                                    <div class="icon-box">
                                        <span class="icon flaticon-avatar"></span>
                                    </div>
                                    <h3><a href="services-single.html">CAPATAZIA</a></h3>
                                    <div class="text">Compreende o recebimento, a conferência, o transporte interno, a abertura de volumes para a conferência aduaneira, a manipulação, a arrumação, a entrega e ainda o carregamento e descarregamento.</div>
                                </div>
                            </div>

                            <!--Service Block One-->
                            <div class="service-block-one">
                                <div class="inner-box">
                                    <div class="icon-box">
                                        <span class="icon flaticon-diamond"></span>
                                    </div>
                                    <h3><a href="services-single.html">ZELADORIA</a></h3>
                                    <div class="text">Compreende em resolver de forma eficiente os problemas de manutenção predial de condomínios residenciais e comerciais.</div>
                                </div>
                            </div>

                            <!--Service Block One-->
                            <div class="service-block-one">
                                <div class="inner-box">
                                    <div class="icon-box">
                                        <span class="icon flaticon-walkie-talkie"></span>
                                    </div>
                                    <h3><a href="services-single.html">PORTARIA</a></h3>
                                    <div class="text">Fiscalização e guarda o patrimônio, além de controlar a entrada e saída de pessoas e veículos nas dependências, bem como orientar as pessoas sobre seus destinos.</div>
                                </div>
                            </div>

                        </div>

                        <!--Right Column-->
                        <div class="right-column col-md-4 col-sm-6 col-xs-12 pull-right">
                            <!--Service Block Two-->
                            <div class="service-block-two">
                                <div class="inner-box">
                                    <div class="icon-box">
                                        <span class="icon flaticon-headphones"></span>
                                    </div>
                                    <h3><a href="services-single.html">OPERADOR DE MÁQUINA</a></h3>
                                    <div class="text">Prepara, ajusta e opera máquinas de produção. Garante a qualidade das máquinas por meio da realização de testes, frequência e padrões estipulados. Mantém a limpeza das máquinas e a organização do setor. Conserva equipamento com a execução de manutenções corretivas e preventivas.</div>
                                </div>
                            </div>

                            <!--Service Block Two-->
                            <div class="service-block-two">
                                <div class="inner-box">
                                    <div class="icon-box">
                                        <span class="icon flaticon-smartphone"></span>
                                    </div>
                                    <h3><a href="services-single.html">RECEPCIONISTA</a></h3>
                                    <div class="text">Profissional responsável pelo atendimento ao público interno e externo, atendimento telefônico, encaminhamento a órgãos e departamentos competentes, além de auxílio a outros setores da organização.</div>
                                </div>
                            </div>

                            <!--Service Block Two-->
                            <div class="service-block-two">
                                <div class="inner-box">
                                    <div class="icon-box">
                                        <span class="icon flaticon-user-1"></span>
                                    </div>
                                    <h3><a href="services-single.html">MÃO DE OBRA TEMPORÁRIA.</a></h3>
                                    <div class="text">A ajuda que faltava para lucrar nos picos sazonais de produção (Natal, Páscoa, Dia das Mães, Dia dos Namorados, Férias Escolares, Locais com Alta Temporada, etc) ou demandas pontuais (Construção Civil, Projetos, etc).</div>
                                </div>
                            </div>

                        </div>

                        <!--Image Column-->
                        <div class="image-column col-md-4 col-sm-12 col-xs-12">
                            <figure class="image-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                                <img src="/images/resource/police-man.png" alt="" />
                            </figure>
                        </div>

                    </div>
                </div>

            </div>
        </section>
        <!--End Services Section-->

        <!--Advantage Section-->
        <section class="advantage-section" style="background-image:url(images/background/1.jpg);">
            <div class="auto-container">
                <!--Sec Title Two-->
                <div class="sec-title-two">
                    <h2>NOSSAS VANTAGENS</h2>
                </div>
                <!--End Sec Title Two-->

                <div class="row clearfix">

                    <!--Advantage Block-->
                    <div class="advantage-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box wow fadeIn" data-wow-delay="0ms" data-wow-duration="1500ms">
                            <figure class="image-box">
                                <a href="services-single.html"><img src="/images/resource/adventure-1.jpg" alt="" /></a>
                            </figure>
                            <div class="lower-content">
                                <div class="inner">
                                    <h3><a href="services-single.html">FOCAR NO CORE BUSINESS</a></h3>
                                    <div class="text">A primeira e mais importante vantagem gerada pela terceirização de serviços é a capacidade de manter gestores focados no core business, mantendo o foco efetivamente no negócio e na estratégia da empresa.</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Advantage Block-->
                    <div class="advantage-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box wow fadeIn" data-wow-delay="300ms" data-wow-duration="1500ms">
                            <figure class="image-box">
                                <a href="services-single.html"><img src="/images/resource/adventure-2.jpg" alt="" /></a>
                            </figure>
                            <div class="lower-content">
                                <div class="inner">
                                    <h3><a href="services-single.html">CONTAR COM ESPECIALISTAS EM TODAS AS ETAPAS DO PROCESSO</a></h3>
                                    <div class="text">Poder contar com a equipe e os profissionais mais especializados e experientes é um benefício muito importante da terceirização, já que as atividades previstas serão realizadas com mais qualidade e excelência.</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Advantage Block-->
                    <div class="advantage-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box wow fadeIn" data-wow-delay="600ms" data-wow-duration="1500ms">
                            <figure class="image-box">
                                <a href="services-single.html"><img src="/images/resource/adventure-3.jpg" alt="" /></a>
                            </figure>
                            <div class="lower-content">
                                <div class="inner">
                                    <h3><a href="services-single.html">REDUZIR CUSTOS OPERACIONAIS</a></h3>
                                    <div class="text">Devido aos encargos e direitos trabalhistas previstos na legislação brasileira, que sobrecarregam a folha de pagamento, as empresas são obrigadas a repensar possíveis contratações. Esses valores representam grandes custos, o que pesa significativamente no orçamento institucional.</div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </section>
        <!--End Advantage Section-->

        <!--Why Us Section-->
        <section id="empresa" class="why-us-section">
            <div class="auto-container">
                <div class="row clearfix">
                    <!--Content Column-->
                    <div class="content-column col-md-6 col-sm-12 col-xs-12">
                        <!--Sec Title Three-->
                        <div class="sec-title-three">
                            <h2>PORQUE ESCOLHER-NOS</h2>
                        </div>
                        <!--End Sec Title Three-->

                        <!--Service Block Two-->
                        <div class="service-block-two style-two">
                            <div class="inner-box">
                                <div class="icon-box">
                                    <span class="icon flaticon-hourglass"></span>
                                </div>
                                <h3><a href="services-single.html">MAIS DE 30 ANOS DE EXPERIÊNCIA.</a></h3>
                                <div class="text">O grupo A.S é constituído por uma equipe com mais de trinta anos de experiência na área de prestação de serviços.</div>
                            </div>
                        </div>

                        <!--Service Block Two-->
                        <div class="service-block-two style-two">
                            <div class="inner-box">
                                <div class="icon-box">
                                    <span class="icon flaticon-user-2"></span>
                                </div>
                                <h3><a href="services-single.html">FUNCIONÁRIOS MOTIVADOS</a></h3>
                                <div class="text">Nossa equipe é continuamente assistida, treinada e motivada, tendo um contato direto com a empresa juntamente com a equipe de supervisão.</div>
                            </div>
                        </div>

                        <!--Service Block Two-->
                        <div class="service-block-two style-two">
                            <div class="inner-box">
                                <div class="icon-box">
                                    <span class="icon flaticon-diamond"></span>
                                </div>
                                <h3><a href="services-single.html">SERVIÇOS DE QUALIDADE</a></h3>
                                <div class="text">Temos um alto grau de satisfação na prestação dos serviços para os nossos clientes.</div>
                            </div>
                        </div>

                    </div>
                    <!--Counter Column-->
                    <div class="counter-column col-md-6 col-sm-12 col-xs-12" style="background-image:url(/images/resource/choose-us.jpg);">

                        <img src="/images/resource/choose-us.jpg" alt="">

                    </div>
                </div>
            </div>
        </section>
        <!--Why Us Section-->

        <!--Call to Action-->
        <section class="call-to-action" style="background-image:url(images/background/2.jpg);">
            <div class="auto-container">
                <div class="row clearfix">
                    <!--Content Column-->
                    <div class="content-column col-md-9 col-sm-9 col-xs-12">
                        <div class="inner-box">
                            <div class="icon-box">
                                <div class="icon flaticon-note"></div>
                            </div>
                            <div class="content">
                                <h3>SOLICITE SEU ORÇAMENTO (OU)  LIGUE-NOS: (85) 3271-0110</h3>
                            </div>
                        </div>
                    </div>

                    <div class="btn-column text-right col-md-3 col-sm-3 col-xs-12">
                        <a href="#orcamento" class="theme-btn btn-style-one">SOLICITAR ORÇAMENTO</a>
                    </div>

                </div>
            </div>
        </section>
        <!--Call to Action-->

        <!--Main Footer-->
        <footer class="main-footer">

            <div class="auto-container">
                <div class="row clearfix">

                    <!--Big Column-->
                    <div class="big-column col-md-5 col-sm-12 col-xs-12">
                        <div class="row clearfix">

                            <!--Footer Column / About Widget-->
                            <div class="footer-column col-md-12 col-sm-12 col-xs-12">
                                <div class="footer-widget about-widget">
                                    <h2>Sobre</h2>
                                    <div class="widget-content">
                                        <div class="text">O grupo A.S. é constituído por uma equipe com mais de trinta anos de experiência na área de prestação de serviços. <br>Nossa equipe é continuamente assistida, treinada e motivada, tendo um contato direto com a empresa juntamente com a equipe de supervisão.</div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <!--Big Column-->
                    <div class="big-column col-md-7 col-sm-12 col-xs-12">
                        <div class="row clearfix">

                            <!--Footer Column / Twitter Widget-->
                            <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                <div class="footer-widget twitter-widget">
                                    <h2>Contato</h2>
                                    <div class="widget-content">
                                        <div class="widget-content">
                                            <ul class="contact-info">
                                                <li><span class="icon flaticon-pin-1"></span> Rua Teofredo Goiana, 1501<br>Cidade dos Funcionários<br>Fortaleza/CE</li>
                                                <li><span class="icon fa fa-envelope-o"></span> contato@asterceirizacao.com.br <br>⠀</li>
                                                <li><span class="icon flaticon-technology-1"></span> 85 3271-0110</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--Footer Column / Contact Widget-->
                            <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                <div class="footer-widget contact-widget">
                                    <h2>NEWSLETTER</h2>
                                    <div class="widget-content">
                                        <form method="post" action="contact.html">

                                            <!--Form Group-->
                                            <div class="form-group">
                                                <div class="group-inner">
                                                    <input type="text" name="nome" placeholder="Nome" required="">
                                                </div>
                                                <div class="group-inner">
                                                    <input type="email" name="email" placeholder="Email" required="">
                                                </div>
                                                <div class="group-inner">
                                                    <button type="submit" class="theme-btn btn-style-one">ENVIAR</button>
                                                </div>
                                            </div>

                                        </form>

                                        <!--Social Icon One-->
                                        <ul class="social-icon-one">
                                            <li><a class="fa fa-facebook" target="_blank" href="https://www.facebook.com/AS-Terceiriza%C3%A7%C3%A3o-245463376666483"></a></li>
                                            <li><a class="fa fa-instagram" target="_blank" href="https://www.instagram.com/asterceirizacao/"></a></li>
                                        </ul>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

            </div>

            <!--Footer Bottom-->
            <div class="footer-bottom">
                <div class="auto-container">
                    <div class="row clearfix">

                        <!--column-->
                        <div class="column col-md-6 col-sm-6 col-xs-12">
                            <div class="copyright"><a href="/Login" class="theme-btn btn-style-one">Acesso ADMIN</a> &copy; {{date('Y')}} A.S. Terceirização. Todos os Direitos Reservados.</div>
                        </div>
                    </div>
                </div>

            </div>

        </footer>

    </div>
    <!--End pagewrapper-->

    <!--Scroll to top-->
    <div class="scroll-to-top scroll-to-target" data-target=".main-header"><span class="fa fa-long-arrow-up"></span></div>

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/revolution.min.js"></script>
    <script src="js/jquery.fancybox.pack.js"></script>
    <script src="js/jquery.fancybox-media.js"></script>
    <script src="js/owl.js"></script>
    <script src="js/wow.js"></script>
    <script src="js/script.js"></script>
</body>
</html>
