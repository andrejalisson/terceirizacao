<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecuperacaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recuperacao', function (Blueprint $table) {
            $table->increments('id_rec');
            $table->string('token_rec', 100)->nullable();
            $table->integer('user_id_rec')->unsigned();
            $table->foreign('user_id_rec')->references('id_user')->on('usuarios');
            $table->dateTime('criacao_rec')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recuperacao');
    }
}
