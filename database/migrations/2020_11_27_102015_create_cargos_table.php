<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCargosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cargos', function (Blueprint $table) {
            $table->increments('id_car');
            $table->integer('cbo_car')->unsigned()->nullable()->default(6);
            $table->string('cargo_car', 60);
            $table->float('salario_car', 8, 2);
            $table->integer('cargaHoraria_car');
            $table->boolean('status_car')->nullable()->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cargos');
    }
}
