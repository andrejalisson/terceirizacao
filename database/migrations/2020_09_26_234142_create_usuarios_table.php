<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->increments('id_user');
            $table->string('nome_user', 100);
            $table->string('senha_user', 72)->nullable();
            $table->string('email_user', 100)->nullable();
            $table->string('username_user', 20)->nullable();
            $table->dateTime('criacao_user')->nullable();
            $table->integer('tentativas_user')->unsigned()->nullable()->default(0);
            $table->boolean('status_user')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
