<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsuarioAdmin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('usuarios')->insert([
            'nome_user' => "André Jálisson",
            'email_user' => "andrejalisson@gmail.com",
            'senha_user' => password_hash("31036700", PASSWORD_DEFAULT),
            'username_user' => "andrejalisson",
            'criacao_user' => date('Y-m-d H:i:s'),
        ]);
    }
}
