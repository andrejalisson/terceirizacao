<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class desbloqueioUsuario extends Mailable
{
    use Queueable, SerializesModels;

    private $usuario;
    private $token;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(\stdClass $usuario, $token){
        $this->usuario = $usuario;
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(){
        $this->subject("Conta Bloqueada");
        $this->to($this->usuario->email_user, $this->usuario->nome_user);
        return $this->view('Mails.desbloqueioUsuario', [
            'usuario' => $this->usuario,
            'token' => $this->token
        ]);
    }
}
