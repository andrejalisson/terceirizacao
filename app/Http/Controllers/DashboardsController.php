<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardsController extends Controller{
    public function dashboard(){
        $title = "Dashboard Administrativo";
        return view('Dashboards.administrativo')->with(compact( 'title'));
    }
}
