<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RHController extends Controller{
    public function cargos(){
        $title = "Cargos e Funções";
        return view('RH.cargos')->with(compact( 'title'));
    }

    public function todasCargos(Request $request){

        $columns = array(
            0 =>'cbo_car',
            1 =>'cargo_car',
            2 =>'salario_car',
            3 =>'cargaHoraria_car',
        );

        $totalData = DB::table('cargos')
                        ->where('status_car', 1)
                        ->count();


        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if(empty($request->input('search.value'))){
            $cargos = DB::table('cargos')
                            ->where('status_car', 1)
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();


        }
        else{
            $search = $request->input('search.value');
            $cargos =  DB::table('cargos')
                            ->where('cargo_car','LIKE',"%{$search}%")
                            ->where('status_car', 1)
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();
            $totalFiltered = DB::table('cargos')
                            ->where('cargo_car','LIKE',"%{$search}%")
                            ->where('status_car', 1)
                            ->count();
        }

        if(!empty($cargos)){
            $data = array();
            foreach ($cargos as $cargo){
                $nestedData['codigo'] = $cargo->id_car;
                $nestedData['nome'] = $cargo->cargo_car;
                $nestedData['salario'] ="R$ ". number_format($cargo->salario_car, 2, ',','.');
                $nestedData['horario'] = $cargo->cargaHoraria_car;
                $nestedData['valorH'] = "R$ ".number_format(round($cargo->salario_car/($cargo->cargaHoraria_car*5), 2), 2, ',','.');
                $nestedData['opcoes'] = "   <a class=\"btn btn-warning btn-circle\" href=\"/EditarCargo/".$cargo->id_car."\" type=\"button\"><i class=\"fa fa-pencil\"></i></a>
                                            <a class=\"btn btn-danger btn-circle\" href=\"/ExcluirCargo/".$cargo->id_car."\" type=\"button\"><i class=\"fa fa-times\"></i></a>";
                $data[] = $nestedData;
            }
        }
        $json_data = array(
                    "draw"            => intval($request->input('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );
        echo json_encode($json_data);
    }
}
