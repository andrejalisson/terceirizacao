<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class AcessoController extends Controller{
    public function login(){
        $title = "Login";
        return view('Acesso.login')->with(compact( 'title'));
    }

    public function forgot(){
        $title = "Esqueceu a senha?";
        return view('Acesso.forgot')->with(compact('title'));
    }

    public function forgotPost(Request $request){
        $usuario = DB::table('usuarios')->where('email_user', $request->usuario)->orwhere('usarname_user', $request->usuario)->first();
        if($usuario != null){
            $request->session()->flash('sucesso', 'Um email foi enviado com as instruções!');
            return redirect('/');
        }else{
            $request->session()->flash('erro', 'Usuário não encontrado.');
            return redirect()->back();
        }
    }

    public function verifica(Request $request){
        $usuario = DB::table('usuarios')->where('email_user', $request->usuario)->orwhere('username_user', $request->usuario)->first();
        if($usuario != null){
            if($usuario->status_user == 1){
                if(password_verify($request->senha, $usuario->senha_user )){
                    $request->session()->flash('sucesso', 'Bom trabalho!');
                    $request->session()->put('id', $usuario->id_user);
                    $request->session()->put('nome', $usuario->nome_user);
                    $request->session()->put('login', $usuario->username_user);
                    $request->session()->put('logado', true);
                    return redirect('/Dashboard');
                }else{
                    switch ($usuario->tentativas_user) {
                        case 0:
                            $request->session()->flash('atencao', 'Senha incorreta! Você tem mais 2 chances.');
                            DB::table('usuarios')
                                ->where('id_user', $usuario->id_user)
                                ->update(['tentativas_user' => 1]);
                            break;
                        case 1:
                            $request->session()->flash('atencao', 'Senha incorreta! Você tem mais 1 chances.');
                            DB::table('usuarios')
                                ->where('id_user', $usuario->id_user)
                                ->update(['tentativas_user' => 2]);
                            break;
                        case 2:
                            $request->session()->flash('erro', 'Senha incorreta! Usuário Bloqueado.');
                            $token = uniqid("BL", true);
                            DB::table('recuperacao')
                                ->insert([
                                    'token_rec' => $token,
                                    'user_id_rec' => $usuario->id_user,
                                    'criacao_rec' => date('Y-m-d H:i:s')
                                ]);
                            DB::table('usuarios')
                                ->where('id_user', $usuario->id_user)
                                ->update([
                                    'tentativas_user' => 3,
                                    'status_user' => 0
                                ]);
                            Mail::send(new \App\Mail\desbloqueioUsuario($usuario, $token));
                            break;
                    }
                    return redirect()->back();
                }
            }else{
                $request->session()->flash('erro', 'Usuário Bloqueado!');
                return redirect()->back();
            }
        }else{
            $request->session()->flash('atencao', 'Usuário não encontrado.');
            return redirect()->back();
        }
    }

    public function verificaToken(Request $request, $token){
        $verifica = DB::table('recuperacao')->where('token_rec', $token)->first();
        if ($verifica != null) {
            $date = new \DateTime($verifica->criacao_rec);
            $now = new \DateTime();
            $intervalo = $date->diff($now)->format("%d");
            if ($intervalo == 0) {
                return view('Acesso.senha')->with(compact('token'));
            }else{
                $request->session()->flash('atencao', 'Token fora da validade.');
                return redirect('/Login');
            }
        }else{
            $request->session()->flash('erro', 'Código Inválido');
            return redirect('/Login');
        }
    }

    public function novasenha(Request $request){
        $verifica = DB::table('recuperacao')->where('token_rec', $request->token)->first();
        if ($verifica != null) {
            $date = new \DateTime($verifica->criacao_rec);
            $now = new \DateTime();
            $intervalo = $date->diff($now)->format("%d");
            if ($intervalo == 0) {
                DB::table('usuarios')
                    ->where('id_user', $verifica->user_id_rec)
                    ->update([
                        'senha_user' => password_hash($request->senha, PASSWORD_DEFAULT),
                        'tentativas_user' => 0,
                        'status_user' => 1
                        ]
                );
                DB::table('recuperacao')->where('token_rec', $request->token)->delete();
                $request->session()->flash('sucesso', 'Faça o Login para continuar!');
                return redirect('/Login');
            }else{
                $request->session()->flash('atencao', 'Token fora da validade.');
                return redirect('/Login');
            }
        }else{
            $request->session()->flash('erro', 'Código Inválido');
            return redirect('/Login');
        }
    }



    public function logout(Request $request){
        session()->flush();
        $request->session()->flash('sucesso', 'Até Logo!');
        return redirect('/Login');
    }
}
