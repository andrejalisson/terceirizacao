<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SiteController extends Controller{
    public function home(){
        $title = "Home";
        return view('Site.home')->with(compact( 'title'));
    }
}
