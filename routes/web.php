<?php

use Illuminate\Support\Facades\Route;

//ACESSO
Route::get('/', 'SiteController@home');
Route::get('/Login', 'AcessoController@login');
Route::post('/Verifica', 'AcessoController@verifica');
Route::get('/Recuperar/{token}', 'AcessoController@verificaToken');
Route::post('/Recuperar', 'AcessoController@novasenha');
Route::get('/EsqueceuSenha', 'AcessoController@forgot');
Route::get('/Sair', 'AcessoController@logout');

//DASHBOARDS
Route::get('/Dashboard', 'DashboardsController@dashboard');

//Configurações
Route::get('/Cargos', 'RHController@cargos');
Route::post('/todasCargos', 'RHController@todasCargos')->name('todasCargos');
